//
//  ViewController.swift
//  Optonaut
//
//  Created by Johannes Schickling on 6/17/15.
//  Copyright © 2015 Optonaut. All rights reserved.
//

import UIKit

class ProfileNavViewController: NavigationController {
    
    required init() {
        super.init(nibName: nil, bundle: nil)
        
        if SessionService.isLoggedIn {
            pushViewController(ProfileCollectionViewController(personID: SessionService.personID), animated: false)
        } else {
            SessionService.loginNotifiaction.signal.observeNext {
                self.popToRootViewControllerAnimated(false)
                print("id ko ito",SessionService.personID)
                
                self.pushViewController(ProfileCollectionViewController(personID: SessionService.personID), animated: false)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        logRetain()
    }
    
}

