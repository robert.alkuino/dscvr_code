//
//  LineTextField.swift
//  Optonaut
//
//  Created by Johannes Schickling on 9/18/15.
//  Copyright © 2015 Optonaut. All rights reserved.
//

import Foundation
import UIKit
import HexColor

class LineTextField: UITextField {
    
    static var i = 0

    enum Status: Equatable {
        case Normal
        case Disabled
        case Indicated
        case Warning(String)
    }
    
    enum Size: CGFloat {
        case Large = 18
        case Medium = 14
        case Small = 13
    }
    
    enum Color {
        case Light
        case Dark
    }
    
    var color: Color = .Dark {
        didSet {
            update()
        }
    }
    
    var size: Size = .Medium {
        didSet {
            layoutSubviews()
        }
    }
    
    var status: Status = .Normal {
        didSet {
            update()
        }
    }
    
    override var placeholder: String? {
        didSet {
            update()
        }
    }
    
    var previousText: String?
    
    override var text: String? {
        didSet {
            previousText = oldValue
        }
    }
    
    override var attributedText: NSAttributedString? {
        didSet {
//            previousText = oldValue
        }
    }
    
    private let lineLayer = CALayer()
    private let messageView = UILabel()
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        postInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func postInit() {
        borderStyle = .None
        backgroundColor = .clearColor()
        clipsToBounds = false
        textAlignment = .Left
        contentVerticalAlignment = .Top
        
        update()
        
        addTarget(self, action: "beginEditing", forControlEvents: .EditingDidBegin)
        addTarget(self, action: "changed", forControlEvents: .EditingChanged)
        addTarget(self, action: "endEditing", forControlEvents: .EditingDidEnd)
        
        messageView.textAlignment = .Right
        addSubview(messageView)
        
        lineLayer.backgroundColor = UIColor.whiteColor().CGColor
        layer.addSublayer(lineLayer)
    }
    
    private func update() {
        let baseColor: UIColor = {
            switch self.color {
            case .Light: return UIColor.whiteColor()
            case .Dark: return UIColor.DarkGrey
            }
        }()
        
        let baseFontSize = size.rawValue
        
        // placeholder
        var attributes: [String: AnyObject] = [
            NSFontAttributeName: UIFont.textOfSize(baseFontSize, withType: .Regular)
        ]
        switch status {
        case .Disabled: attributes[NSForegroundColorAttributeName] = baseColor.alpha(0.15)
        default: attributes[NSForegroundColorAttributeName] = baseColor.alpha(0.4)
        }
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: attributes)
        
        font = UIFont.textOfSize(baseFontSize, withType: .Regular)
        
        // text field
        textColor = baseColor
        if case .Disabled = status {
            userInteractionEnabled = false
        } else {
            userInteractionEnabled = true
        }
        
        // line color
        if case .Light = color {
            switch status {
            case .Disabled: lineLayer.backgroundColor = baseColor.alpha(0.15).CGColor
            case .Indicated: lineLayer.backgroundColor = baseColor.alpha(0.7).CGColor
            default: lineLayer.backgroundColor = baseColor.alpha(1).CGColor
            }
        } else {
            switch status {
            case .Disabled: lineLayer.backgroundColor = baseColor.alpha(0.15).CGColor
            case .Indicated: lineLayer.backgroundColor = UIColor.Accent.CGColor
            default: lineLayer.backgroundColor = baseColor.alpha(1).CGColor
            }
        }
        
        // message
        if case .Light = color {
            messageView.textColor = baseColor
        } else {
            messageView.textColor = baseColor
        }
        messageView.font = UIFont.displayOfSize(10, withType: .Regular)
        if case .Warning(let message) = status {
            messageView.text = message
        } else {
            messageView.text = ""
        }
        
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let onePx = 1 / UIScreen.mainScreen().scale
        
        switch size {
        case .Large:
            frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: 51))
            lineLayer.frame = CGRect(x: 0, y: 34, width: frame.width, height: onePx)
            messageView.frame = CGRect(x: 0, y: 36, width: frame.width, height: 15)
        case .Medium:
            frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: 43))
            lineLayer.frame = CGRect(x: 0, y: 28, width: frame.width, height: onePx)
            messageView.frame = CGRect(x: 0, y: 30, width: frame.width, height: 15)
        case .Small:
            frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: 41))
            lineLayer.frame = CGRect(x: 0, y: 24, width: frame.width, height: onePx)
            messageView.frame = CGRect(x: 0, y: 26, width: frame.width, height: 15)
        }
    }
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        let margin: CGFloat = 5
        let area = CGRectInset(bounds, -margin, -margin)
        return CGRectContainsPoint(area, point)
    }
    
    func beginEditing() {
        update()
    }
    
    func endEditing() {
        update()
    }
    
}

func ==(lhs: LineTextField.Status, rhs: LineTextField.Status) -> Bool {
    switch (lhs, rhs) {
    case let (.Warning(lhs), .Warning(rhs)): return lhs == rhs
    case (.Normal, .Normal): return true
    case (.Disabled, .Disabled): return true
    case (.Indicated, .Indicated): return true
    default: return false
    }
}