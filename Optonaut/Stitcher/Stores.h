#include "checkpointStore.hpp"

class Stores {
public:
    static optonaut::CheckpointStore left;
    static optonaut::CheckpointStore right;
    static optonaut::CheckpointStore common;
    static optonaut::CheckpointStore debug;
};